"""
The geotiff is over 350MB and needs a library that can handle it.
Both of these things make it a challenge to work with.
Windows in particular lacks a Python library that can read geotiffs
out of the box.

To work around this, this script reads in the original, dowsamples it
to about 1% of its original size, and saves it as in a NumPy format.
"""
import os
import numpy as np
import rasterio

# The number of rows and columns in the downsampled image.
# The original has 10,012.
new_size = 1024

geotiff_filename = os.path.join(
    "data", "USGS_one_meter_x46y404_NM_CO_Southern_San_Luis_TL_2015.tif")

with rasterio.open(geotiff_filename) as src:
    # By default, the data returned is in a 3D array, but this geotiff
    # only has one channel.
    # Explicity reduce the 0th channel to a 2D array.
    elevation = src.read()[0, :, :]

# Do some downsampling.
n_rows, n_cols = elevation.shape
# Find the row and column indices that will constitute the
# downsampled grid.
y_recon = [int(x) for x in np.linspace(0, n_rows - 1, new_size)]
x_recon = [int(x) for x in np.linspace(0, n_cols - 1, new_size)]

# Expand the row and column locations to a full grid.
X, Y = np.meshgrid(x_recon, y_recon)
downsampled_elevation = elevation[Y, X]

np.save(os.path.join("data", "downsampled_elevation.npy"),
        downsampled_elevation)
