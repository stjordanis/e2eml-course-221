"""
Use k-nearest neighbors to estimate the price of diamonds.
The data is downloaded from Kaggle
https://www.kaggle.com/shivam2503/diamonds
"""
import os
import numpy as np

np.random.seed(645839)

k = 7
training_fraction = .8
n_splits = 10

# How many data points to use.
# If this number is larger than the full data set size (about 65K), then
# the full data set will be used.
n_pts_max = 10000

data_filename = os.path.join("data", "diamonds.csv")


def main():
    """
    Load the data, split it, and evaluate a k-NN model on it.
    """
    features, labels = load_data()

    scores = []
    for i in range(n_splits):
        (train_features,
            train_labels,
            test_features,
            test_labels) = prep_data(features, labels)

        n_test = test_labels.size
        total_score = 0
        for i_test in range(n_test):
            i_top, distances = find_k_nearest(
                train_features, test_features[i_test, :])
            score = score_examples(
                train_labels[i_top], distances, test_labels[i_test])
            total_score += score
        iter_score = total_score / n_test
        scores.append(iter_score)

    final_score = np.mean(scores)
    print(f"best score: {final_score}")


def load_data():
    """
    Retrieve the diamonds data from a csv file, pull out the useful bits,
    and convert it all to numbers.

    Returns
    features, 2D NumPy array of floats
    labels, 1D NumPy array of floats
    """
    # column 0 is row number
    # column 7 is price

    # Create ordered lists for cut, color, and clarity, so that their values
    # can be converted into numbers based on their position in the list.
    # column 2 cut
    col2_labels = ['"Fair"', '"Good"', '"Very Good"', '"Premium"', '"Ideal"']
    # column 3 color
    col3_labels = ['"J"', '"I"', '"H"', '"G"', '"F"', '"E"', '"D"']
    # column 4 clarity
    col4_labels = [
        '"I1"', '"SI2"', '"SI1"', '"VS2"', '"VS1"', '"VVS2"', '"VVS1"', '"IF"']

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # column_labels = data_lines[0].split(",")

        raw_data = []
        # Ignore the row with the column labels.
        for line in data_lines[1:]:
            line_strings = line.split(",")
            # Convert cut, color, and clarity strings into numbers, based
            # on their position in their respective lists.
            line_strings[2] = col2_labels.index(line_strings[2])
            line_strings[3] = col3_labels.index(line_strings[3])
            line_strings[4] = col4_labels.index(line_strings[4])
            # Leave off the row number by starting at column 1.
            # Convert everything to floats.
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)
        raw_data = np.array(raw_data)

        # Segregate out the features from the prices.
        features = raw_data[:, [0, 1, 2, 3, 4, 5, 7, 8, 9]]
        labels = raw_data[:, 6]
        return features, labels


def prep_data(features, labels):
    """
    Downselect, pre-process, and split the data.

    Arguments
    features, 2D NumPy array of floats
    labels, 1D NumPy array of floats

    Returns
    train_features, 2D NumPy array of floats
    train_labels, 1D NumPy array of floats
    test_features, 2D NumPy array of floats
    test_labels, 1D NumPy array of floats
    """
    # Randomly pull a subset of the data to work with, if n_pts is
    # smaller than the number of points in the data set.
    # This helps the model to run faster during development.
    n_pts_total = features.shape[0]
    n_pts = np.minimum(n_pts_max, n_pts_total)
    i_pts = np.arange(n_pts_total)
    np.random.shuffle(i_pts)
    i_keep = i_pts[:n_pts]
    keeper_features = features[i_keep, :]
    keeper_labels = labels[i_keep]

    # Randomly split the data into training and testing sets.
    n_train = int(n_pts * training_fraction)
    straws = np.arange(n_pts)
    np.random.shuffle(straws)
    i_train = straws[:n_train]
    i_test = straws[n_train:]

    # Calculate mean and stddev using the training data points.
    # Use those to normalize both training and testing feature sets.
    unscaled_train_features = keeper_features[i_train, :]
    unscaled_test_features = keeper_features[i_test, :]
    features_mean = np.mean(unscaled_train_features, axis=0)
    features_stddev = np.sqrt(np.var(unscaled_train_features, axis=0))
    epsilon = 1e-3
    train_features = (
        unscaled_train_features - features_mean) / (features_stddev + epsilon)
    test_features = (
        unscaled_test_features - features_mean) / (features_stddev + epsilon)

    train_labels = keeper_labels[i_train]
    test_labels = keeper_labels[i_test]

    return train_features, train_labels, test_features, test_labels


def find_k_nearest(train_points, test_point):
    """
    Find the distance between the test point and each of the training points.
    Use the Manhattan distance, the sum of differences in each dimension.

    Arguments
    train_points, 2D NumPy array of floats
    test_point, 1D NumPy array of floats

    Returns
    i_top_k, 1D NumPy array of floats
    distance_top_k, 1D NumPy array of floats
    """
    # The np.newaxises convert the 1D arrays to 2D and make sure the
    # values are arrayed across columns (shape 1 x N) rather than
    # across rows (shape N x 1).
    distance = np.sum(
        np.abs(train_points - test_point[np.newaxis, :]), axis=1)
    # Get the k closest points.
    order = np.argsort(distance)
    i_top_k = order[:k]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k


def score_examples(labels, distances, actual):
    """
    Use the neighboring values and their distances from the test point
    to make a prediction, then compare that with the actual value to
    calculate a score.

    Arguments
    labels, 1D NumPy array of floats
    distances, 1D NumPy array of floats
    actual, float

    Returns
    score, float
    """
    # Create a distance floor, a minimum distance.
    # This prevents single data points from absolutely dominating when a
    # test point comes in with the same characteristics.
    offset = 1e-2

    # Take a weighted average of the neighboring prices.
    # Weight them by the inverse of their distance from the test point.
    prediction = np.average(labels, weights=1/(distances + offset))

    # The score is the negative of the absolute value of the difference
    # between the actual and predicted values. Overestimates are treated
    # the same as underestimates.
    score = -np.abs(actual - prediction)
    return score


if __name__ == "__main__":
    main()
