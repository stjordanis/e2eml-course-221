"""
Data downloaded from
Staged Products Directory
The National Map
United States Geological Survey
US Department of the Interior
https://prd-tnm.s3.amazonaws.com/index.html?prefix=StagedProducts/Elevation/1m/Projects/NM_CO_Southern_San_Luis_TL_2015/TIFF/

The data covers a square parcel, a little over 10 km on a side.
Elevation is reported every meter in both the East-West direction (columns)
and the North-South direction (rows).
Locations missing data have a physically nonsensical
large negative number assigned to them.
"""
import os
import numpy as np

elevation_filename = os.path.join("data", "downsampled_elevation.npy")


def main():
    """
    Read in the elevation data, downsample it, reconstruct it, visualize it.
    """
    full_elevation = read_elevation_data()
    print(full_elevation[:10, :10])
    print(np.min(full_elevation))


def read_elevation_data():
    """
    The elevation data is in meters, and is sampled
    at about 10 meter intervals in both directions.

    Returns
    elevation, a 2D NumPy array of floats
    """
    elevation = np.load(elevation_filename)
    return elevation


if __name__ == "__main__":
    main()
