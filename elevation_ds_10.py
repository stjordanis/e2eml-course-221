"""
Data downloaded from
Staged Products Directory
The National Map
United States Geological Survey
US Department of the Interior
https://prd-tnm.s3.amazonaws.com/index.html?prefix=StagedProducts/Elevation/1m/Projects/NM_CO_Southern_San_Luis_TL_2015/TIFF/

The data covers a square parcel, a little over 10 km on a side.
Elevation is reported every meter in both the East-West direction (columns)
and the North-South direction (rows).
Locations missing data have a physically nonsensical
large negative number assigned to them.
"""
import os
import numpy as np
from numba import njit
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa F401
animate = False
try:
    from lodgepole import animation_tools as at
    animate = True
    os.makedirs("images", exist_ok=True)
except Exception:
    print("Failed to import the lodgepole package")
    print("Visit https://e2eml.school/lodgepole")


# The total number of samples to gather.
n_samples = 2 ** 6

# The number of rows and columns in the reconstructed version of the
# elevation data.
reconstructed_size = 2 ** 8

# The number of rows and columns in the original data set.
original_size = 1024

elevation_filename = os.path.join("data", "downsampled_elevation.npy")
elevation_range_min = 2500
elevation_range_max = 3400

k = 9


def main():
    """
    Read in the elevation data, sample one point, reconstruct it, repeat.
    Combine the images from each step into a video.
    """
    full_elevation = read_elevation_data()
    sampled_elevation = None
    for i_sample in range(n_samples):
        new_sample = get_single_sample(full_elevation)
        if sampled_elevation is None:
            sampled_elevation = new_sample
        else:
            sampled_elevation = np.concatenate(
                (sampled_elevation, new_sample), axis=0)
        (x_recon, y_recon, reconstructed_elevation) = \
            reconstruct(sampled_elevation)
        visualize(x_recon, y_recon, reconstructed_elevation, i_sample)
    if animate:
        try:
            at.stitch(dirname="images", mp4filename="elevation_sampling.mp4")
        except Exception:
            print("Failed to stitch images into a video.")


def read_elevation_data():
    """
    The elevation data is in meters, and is sampled
    at about 10 meter intervals in both directions.
    Replace instances of missing data with the lowest elevation present
    in the non-missing data.

    Returns
    elevation, a 2D NumPy array of floats
    """
    elevation = np.load(elevation_filename)

    # Find the indices of the missing data.
    i_undefined = np.where(elevation < 0)
    # Change the missing data to a physically implausible large value.
    elevation[i_undefined] = 1e6
    # Find the lowest value of non-missing elevation.
    elevation_floor = np.min(elevation)
    # Change the missing data to match that.
    elevation[i_undefined] = elevation_floor
    return elevation


def get_single_sample(elevation):
    """
    Choose a random sample from the original elevation data.

    The original data is a 2D array of elevation values in meters.
    It's understood that each row index and column index does double
    duty representing that element's location in meters. This is thanks
    to the convenient fact that the data was collected at a 1 meter
    resolution.

    The returned sample will have the structure
    [[x_position, y_position, elevation]]

    Arguments
    elevation, a 2D NumPy array of floats

    Returns
    sampled_elevation, a [1 x 3] 2D NumPy array of floats
    """
    # Forgive the index finagling here.
    # All this is to construct the indices for downsampling.
    n_rows, n_cols = elevation.shape
    # Find the row and column indices of the sample.
    i_row = np.random.randint(n_rows)
    i_col = np.random.randint(n_cols)
    elevation_value = elevation[i_row, i_col]
    x = i_col
    y = i_row

    # Assemble x, y, and elevation for sample into a 1 x 3 array.
    elevation_sample = np.array([[x, y, elevation_value]])
    return elevation_sample


def reconstruct(sampled_elevation):
    """
    Reconstruct the image using the samples.
    The reconstructed image is known to be a grid, but unlike the
    original data, the rows and columns aren't in 1 meter increments.
    To capture this, the positions of each row and column of estimates
    in meters is returned, together with the array of interpolated
    elevations.

    See get_single_sample() for details on the structure of the
    input argument.

    Arguments
    sampled_elevation, a 2D NumPy array of floats

    Returns
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats
    """
    # Find the x- and y-positions in meters of the columns and rows
    # of the reconstruction.
    x_recon = np.linspace(0, original_size, reconstructed_size, dtype=int)
    y_recon = np.linspace(0, original_size, reconstructed_size, dtype=int)

    # Create an empty reconstruction of the right size.
    reconstructed_elevation = np.zeros(
        (reconstructed_size, reconstructed_size))

    # Make sure, for very coarse reconstructions, that k doesn't exceed
    # the size of the downsampled data.
    k_corrected = np.minimum(sampled_elevation.shape[0], k)

    interpolate(
        k_corrected,
        x_recon,
        y_recon,
        sampled_elevation,
        reconstructed_elevation)
    return x_recon, y_recon, reconstructed_elevation


@njit
def interpolate(
    k,
    x_recon,
    y_recon,
    sampled_elevation,
    reconstructed_elevation
):
    """
    Use the samples to find the estimated elevation.

    This is the computational bottleneck, so it's accelerated with
    a call to Numba's njit just-in-time compiler decorator.

    To ensure that it runs smoothly, it can help to pass in all the
    variables we'll need already initialized. This saves Numba from
    having to guess exactly what type they're going to be ahead of time.
    Numba *usually* does a good job at this, but when I do have trouble
    with Numba, it tends to be because it is struggling to infer type.

    Arguments
    k, int
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    sampled_elevation, a 2D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats

    Returns
    No explicit returns. Instead input argument
    reconstructed_elevation is modified so that it contains the estimates.
    """
    # Step through each point in the grid of reconstructed elevation estimates.
    for j_col, x_r in enumerate(x_recon):
        for i_row, y_r in enumerate(y_recon):

            # Find the Euclidean (as the crow flies) distance between the
            # point to estimate and all the samples.
            sample_positions = sampled_elevation[:, :2]
            position_differences = sample_positions - np.array([[x_r, y_r]])
            distance = np.sqrt(np.sum(position_differences ** 2, axis=1))

            # Pick out the the closest k samples and their distances.
            i_top_k = np.argsort(distance)[:k]
            distance_top_k = distance[i_top_k]
            elevation_top_k = sampled_elevation[i_top_k, 2]

            # Find a weighted average of the k closest samples.
            # Taking the log of the distance gives us a scale-independent
            # version of the distance. It will help kNN work well across
            # a wide range of sampling densities.
            log_distance = np.log(distance_top_k + 1)
            weights = 1 / (log_distance + 1)
            reconstructed_elevation[i_row, j_col] = \
                np.sum(elevation_top_k * weights) / np.sum(weights)


def visualize(x_recon, y_recon, reconstructed_elevation, i_sample):
    """
    Turn the reconstructed elevation into a 3D plot.

    Arguments
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats
    i_sample, int
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Expand the row and column locations to a full grid.
    X, Y = np.meshgrid(x_recon, y_recon)
    # Account for the convention that rows are numbered top to bottom.
    Y = np.flipud(Y)

    # Plot the surface, clean the axes, and add a colorbar.
    surf = ax.plot_surface(
        X, Y, reconstructed_elevation, cmap="turbo",
        vmin=elevation_range_min,
        vmax=elevation_range_max,
        linewidth=0, antialiased=True)
    ax.set_zlim3d(elevation_range_min, elevation_range_max)
    ax.set_title(f"Elevation reconstructed from {i_sample} samples")
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)

    reconstructed_image_filename = os.path.join(
        "images", f"reconstructed_elevation_{i_sample:05d}.png")
    fig.savefig(reconstructed_image_filename, dpi=300)
    plt.close()


if __name__ == "__main__":
    main()
