"""
Use k-nearest neighbors to identify penguins.
The data is downloaded from
https://raw.githubusercontent.com/mcnakhaee/palmerpenguins/master/palmerpenguins/data/penguins.csv
and the data set is metculously documented here
https://github.com/allisonhorst/palmerpenguins/blob/master/README.md

Data citation:
   Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer
   Archipelago (Antarctica) penguin data. R package version 0.1.0.
   https://allisonhorst.github.io/palmerpenguins/. doi:
   10.5281/zenodo.3960218.

License: CC0 Public Domain
"""
import os

data_filename = os.path.join("data", "penguins.csv")

with open(data_filename, "rt") as f:
    data_lines = f.readlines()
    for line in data_lines:
        print(line)
